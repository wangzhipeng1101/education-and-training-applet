"use strict";
Object.defineProperties(exports, { __esModule: { value: true }, [Symbol.toStringTag]: { value: "Module" } });
const common_vendor = require("./common/vendor.js");
if (!Math) {
  "./pages/index/index.js";
  "./pages/home/home.js";
  "./pages/login/login.js";
  "./pages/courseClass/courseClass.js";
  "./pages/courseDetails/courseDetails.js";
  "./pages/courseDetails/children/commentAll.js";
  "./pages/learningCenter/learningCenter.js";
  "./pages/mine/mine.js";
  "./pages/learningCenter/Examation/Examation.js";
  "./pages/learningCenter/Examation/ExamList/ExamList.js";
  "./pages/learningCenter/Examation/ExamList/ExamData/ExamData.js";
}
const _sfc_main = {
  onLaunch: function() {
    console.log("App Launch");
  },
  onShow: function() {
    console.log("App Show");
  },
  onHide: function() {
    console.log("App Hide");
  }
};
const App = /* @__PURE__ */ common_vendor._export_sfc(_sfc_main, [["__file", "C:/Users/Hasee/Desktop/\u524D\u53F0/education-and-training-applet/App.vue"]]);
function createApp() {
  const app = common_vendor.createSSRApp(App);
  return {
    app
  };
}
createApp().app.mount("#app");
exports.createApp = createApp;
